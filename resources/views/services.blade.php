@extends('layouts.app')

@section('content')

    <div class="theme-inner-banner">
        <div class="opacity">
            <div class="container">
                <h2 class="float-left">Our Service</h2>
                <ul class="float-right">
                    <li><a href="#" class="tran3s">Home</a></li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li>Our Service</li>
                </ul>
            </div>
        </div> <!-- /.opacity -->
    </div> <!-- /.theme-inner-banner -->




    <!--
    =============================================
        Our Service
    ==============================================
    -->
    <div class="our-service style-two">
        <div class="container">
            <div class="seo-title-one text-center">
                <h2>We Provide <br>Wide Range of SEO Service</h2>
                <h6>We have all type SEO &amp; Marketing of service for our customer</h6>
            </div> <!-- /.seo-title-one -->

            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp">
                    <div class="single-service tran4s text-center">
                        <img src="{{ asset('images/icon/2.png') }}" alt="icon">
                        <h5><a href="#" class="tran4s">Google Analyzing</a></h5>
                        <p>There are many variations Duis mass lacus suad id. sodales scelerisque have suffered alter
                            ation in some form seeker</p>
                        <a href="#" class="tran3s more">More Details <i class="fa fa-caret-right"
                                                                                           aria-hidden="true"></i></a>
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="single-service tran4s text-center">
                        <img src="{{ asset('images/icon/3.png') }}" alt="icon">
                        <h5><a href="#" class="tran4s">Content Research</a></h5>
                        <p>There are many variations Duis mass lacus suad id. sodales scelerisque have suffered alter
                            ation in some form seeker</p>
                        <a href="#" class="tran3s more">More Details <i class="fa fa-caret-right"
                                                                                           aria-hidden="true"></i></a>
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="single-service tran4s text-center">
                        <img src="{{ asset('images/icon/4.png') }}" alt="icon">
                        <h5><a href="#" class="tran4s">Keyword Suggestion</a></h5>
                        <p>There are many variations Duis mass lacus suad id. sodales scelerisque have suffered alter
                            ation in some form seeker</p>
                        <a href="#" class="tran3s more">More Details <i class="fa fa-caret-right"
                                                                                           aria-hidden="true"></i></a>
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="single-service tran4s text-center">
                        <img src="{{ asset('images/icon/14.png') }}" alt="icon">
                        <h5><a href="#" class="tran4s">link Optimization</a></h5>
                        <p>There are many variations Duis mass lacus suad id. sodales scelerisque have suffered alter
                            ation in some form seeker</p>
                        <a href="#" class="tran3s more">More Details <i class="fa fa-caret-right"
                                                                                           aria-hidden="true"></i></a>
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="single-service tran4s text-center">
                        <img src="{{ asset('images/icon/15.png') }}" alt="icon">
                        <h5><a href="#" class="tran4s">SEO Optimization</a></h5>
                        <p>There are many variations Duis mass lacus suad id. sodales scelerisque have suffered alter
                            ation in some form seeker</p>
                        <a href="#" class="tran3s more">More Details <i class="fa fa-caret-right"
                                                                                           aria-hidden="true"></i></a>
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="single-service tran4s text-center">
                        <img src="{{ asset('images/icon/2.png') }}" alt="icon">
                        <h5><a href="#" class="tran4s">Social Media Marketing</a></h5>
                        <p>There are many variations Duis mass lacus suad id. sodales scelerisque have suffered alter
                            ation in some form seeker</p>
                        <a href="#" class="tran3s more">More Details <i class="fa fa-caret-right"
                                                                                           aria-hidden="true"></i></a>
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="single-service tran4s text-center">
                        <img src="{{ asset('images/icon/16.png') }}" alt="icon">
                        <h5><a href="#" class="tran4s">Mobile Optimization</a></h5>
                        <p>There are many variations Duis mass lacus suad id. sodales scelerisque have suffered alter
                            ation in some form seeker</p>
                        <a href="#" class="tran3s more">More Details <i class="fa fa-caret-right"
                                                                                           aria-hidden="true"></i></a>
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="single-service tran4s text-center">
                        <img src="{{ asset('images/icon/17.png') }}" alt="icon">
                        <h5><a href="#" class="tran4s">Email Maketing</a></h5>
                        <p>There are many variations Duis mass lacus suad id. sodales scelerisque have suffered alter
                            ation in some form seeker</p>
                        <a href="#" class="tran3s more">More Details <i class="fa fa-caret-right"
                                                                                           aria-hidden="true"></i></a>
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="single-service tran4s text-center">
                        <img src="{{ asset('images/icon/3.png') }}" alt="icon">
                        <h5><a href="#" class="tran4s">SEO Audit &amp; Review</a></h5>
                        <p>There are many variations Duis mass lacus suad id. sodales scelerisque have suffered alter
                            ation in some form seeker</p>
                        <a href="#" class="tran3s more">More Details <i class="fa fa-caret-right"
                                                                                           aria-hidden="true"></i></a>
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
            </div>
        </div> <!-- /.container -->
    </div> <!-- /.our-service -->

@stop