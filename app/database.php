<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class database extends Model
{
    public function school()
    {
        return $this->belongsTo(School::class, 'school_id', 'id');
    }
}
